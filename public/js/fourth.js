


var Pacman = function(game, key) {   
    this.game = game;
    this.key = key;
    
    //this.speed = 150;
    this.speed = 0;
	this.isDead = false;
    this.isAnimatingDeath = false;
    this.keyPressTimer = 0;
    
    this.gridsize = this.game.gridsize;
    this.safetile = this.game.safetile;

    this.marker = new Phaser.Point();
    this.turnPoint = new Phaser.Point();
    this.threshold = 6;

    this.directions = [ null, null, null, null, null ];
    this.opposites = [ Phaser.NONE, Phaser.RIGHT, Phaser.LEFT, Phaser.DOWN, Phaser.UP ];

    this.current = Phaser.NONE;
    this.turning = Phaser.NONE;
    this.want2go = Phaser.NONE;
    
    this.keyPressTimer = 0;
    this.KEY_COOLING_DOWN_TIME = 750;
    
    //  Position Pacman at grid location 14x17 (the +8 accounts for his anchor)
    this.sprite = this.game.add.sprite((14 * 16) + 8, (17 * 16) + 8, key, 0);
    this.sprite.anchor.setTo(0.5);
    //this.sprite.animations.add('munch', [0, 1, 2, 1], 20, true);
	//this.sprite.animations.add('munch', [23, 24], 7, true);
	this.sprite.animations.add('munch', [0, 1], 7, true);
    //this.sprite.animations.add("death", [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 10, false);
    //this.sprite.animations.add("death", [73,74,75,76], 10, false); //wrong
    this.sprite.animations.add("death", [2,3,4,5,6], 10, false);
	this.game.physics.arcade.enable(this.sprite);
    this.sprite.body.setSize(16, 16, 0, 0);
    
    this.sprite.play('munch');
	//this.move(Phaser.LEFT);  
    this.move(Phaser.NONE);    
	
};

Pacman.prototype.move = function(direction) {
    if (direction === Phaser.NONE) {
        this.sprite.body.velocity.x = this.sprite.body.velocity.y = 0;
        return;
    }
    var speed = 150;

    if (direction === Phaser.LEFT || direction === Phaser.UP)
    {
        speed = -speed;
    }

    if (direction === Phaser.LEFT || direction === Phaser.RIGHT)
    {
        this.sprite.body.velocity.x = speed;
    }
    else
    {
        this.sprite.body.velocity.y = speed;
    }

    //  Reset the scale and angle (Pacman is facing to the right in the sprite sheet)
    this.sprite.scale.x = 1;
    this.sprite.angle = 0;

    if (direction === Phaser.LEFT)
    {
        this.sprite.scale.x = -1;
    }
    else if (direction === Phaser.UP)
    {
        this.sprite.angle = 270;
    }
    else if (direction === Phaser.DOWN)
    {
        this.sprite.angle = 90;
    }

    this.current = direction;
};

Pacman.prototype.update = function() {
    if (!this.isDead) {
        this.game.physics.arcade.collide(this.sprite, this.game.layer);
        this.game.physics.arcade.overlap(this.sprite, this.game.dots, this.eatDot, null, this);
        this.game.physics.arcade.overlap(this.sprite, this.game.pills, this.eatPill, null, this);

        this.marker.x = this.game.math.snapToFloor(Math.floor(this.sprite.x), this.gridsize) / this.gridsize;
        this.marker.y = this.game.math.snapToFloor(Math.floor(this.sprite.y), this.gridsize) / this.gridsize;

        if (this.marker.x < 0) {
            this.sprite.x = this.game.map.widthInPixels - 1;
        }
        if (this.marker.x >= this.game.map.width) {
            this.sprite.x = 1;
        }

        //  Update our grid sensors
        this.directions[1] = this.game.map.getTileLeft(this.game.layer.index, this.marker.x, this.marker.y);
        this.directions[2] = this.game.map.getTileRight(this.game.layer.index, this.marker.x, this.marker.y);
        this.directions[3] = this.game.map.getTileAbove(this.game.layer.index, this.marker.x, this.marker.y);
        this.directions[4] = this.game.map.getTileBelow(this.game.layer.index, this.marker.x, this.marker.y);

        if (this.turning !== Phaser.NONE)
        {
            this.turn();
        }
    } else {
        this.move(Phaser.NONE);
        if (!this.isAnimatingDeath) {
            this.sprite.play("death");
            this.isAnimatingDeath = true;
        }
    }
};

Pacman.prototype.checkKeys = function(cursors) {
    if (cursors.left.isDown ||
        cursors.right.isDown ||
        cursors.up.isDown ||
        cursors.down.isDown) {
        this.keyPressTimer = this.game.time.time + this.KEY_COOLING_DOWN_TIME;
    }

    if (cursors.left.isDown && this.current !== Phaser.LEFT)
    {
        this.want2go = Phaser.LEFT;
    }
    else if (cursors.right.isDown && this.current !== Phaser.RIGHT)
    {
        this.want2go = Phaser.RIGHT;
    }
    else if (cursors.up.isDown && this.current !== Phaser.UP)
    {
        this.want2go = Phaser.UP;
    }
    else if (cursors.down.isDown && this.current !== Phaser.DOWN)
    {
        this.want2go = Phaser.DOWN;
    }

    if (this.game.time.time > this.keyPressTimer)
    {
        //  This forces them to hold the key down to turn the corner
        this.turning = Phaser.NONE;
        this.want2go = Phaser.NONE;
    } else {
        this.checkDirection(this.want2go);    
    }
};

Pacman.prototype.eatDot = function(pacman, dot) {
    dot.kill();
    this.game.score ++;
    this.game.numDots --;

    if (this.game.dots.total === 0)
    {
        this.game.dots.callAll('revive');
    }
};

Pacman.prototype.eatPill = function(pacman, pill) {
    pill.kill();
    
    this.game.score ++;
    this.game.numPills --;
    
    this.game.enterFrightenedMode();
};

Pacman.prototype.turn = function () {
    var cx = Math.floor(this.sprite.x);
    var cy = Math.floor(this.sprite.y);

    //  This needs a threshold, because at high speeds you can't turn because the coordinates skip past
    if (!this.game.math.fuzzyEqual(cx, this.turnPoint.x, this.threshold) || !this.game.math.fuzzyEqual(cy, this.turnPoint.y, this.threshold))
    {
        return false;
    }

    //  Grid align before turning
    this.sprite.x = this.turnPoint.x;
    this.sprite.y = this.turnPoint.y;

    this.sprite.body.reset(this.turnPoint.x, this.turnPoint.y);
    this.move(this.turning);
    this.turning = Phaser.NONE;

    return true;
};

Pacman.prototype.checkDirection = function (turnTo) {
    if (this.turning === turnTo || this.directions[turnTo] === null || this.directions[turnTo].index !== this.safetile)
    {
        //  Invalid direction if they're already set to turn that way
        //  Or there is no tile there, or the tile isn't index 1 (a floor tile)
        return;
    }

    //  Check if they want to turn around and can
    if (this.current === this.opposites[turnTo])
    {
        this.move(turnTo);
        this.keyPressTimer = this.game.time.time;
    }
    else
    {
        this.turning = turnTo;

        this.turnPoint.x = (this.marker.x * this.gridsize) + (this.gridsize / 2);
        this.turnPoint.y = (this.marker.y * this.gridsize) + (this.gridsize / 2);
        this.want2go = Phaser.NONE;
    }
};

Pacman.prototype.getPosition = function () {
    return new Phaser.Point((this.marker.x * this.gridsize) + (this.gridsize / 2), (this.marker.y * this.gridsize) + (this.gridsize / 2));
};

Pacman.prototype.getCurrentDirection = function() {
    return this.current;
};

var Ghost = function(game, key, name, startPos, startDir) {
    this.game = game;
    this.key  = key;
    this.name = name;
    
    this.ORIGINAL_OVERFLOW_ERROR_ON = this.game.ORIGINAL_OVERFLOW_ERROR_ON;
    
    this.gridsize = this.game.gridsize;
    this.safetiles = [this.game.safetile, 35, 36];
    this.startDir = startDir;
    this.startPos = startPos;
    this.threshold = 6;
    
    this.turnTimer = 0;
    this.TURNING_COOLDOWN = 150;
    this.RETURNING_COOLDOWN = 100;
    this.RANDOM     = "random";
    this.SCATTER    = "scatter";
    this.CHASE      = "chase";
    this.STOP       = "stop";
    this.AT_HOME    = "at_home";
    this.EXIT_HOME  = "leaving_home";
    this.RETURNING_HOME = "returning_home";
    this.isAttacking = false;
    
    this.mode = this.AT_HOME;
    this.scatterDestination = new Phaser.Point(27 * this.gridsize, 30 * this.gridsize);
    
    this.ghostSpeed = 150;
    this.ghostScatterSpeed = 125;
    this.ghostFrightenedSpeed = 75;
    this.cruiseElroySpeed = 160;
    this.directions = [ null, null, null, null, null ];
    this.opposites = [ Phaser.NONE, Phaser.RIGHT, Phaser.LEFT, Phaser.DOWN, Phaser.UP ];
    this.currentDir = startDir;
    
    this.turnPoint = new Phaser.Point();
    this.lastPosition = { x: -1, y: -1 };
    
    //var offsetGhost = 0;
    var offsetGhost = 4;
	switch (this.name) {
        case "clyde":
            //offsetGhost = 4;
			offsetGhost = 8;
            this.scatterDestination = new Phaser.Point(0, 30 * this.gridsize);
            break;
        case "pinky":
            //offsetGhost = 8;
            offsetGhost = 12;
			this.scatterDestination = new Phaser.Point(0, 0);
            break;
        case "blinky":
            //offsetGhost = 12;
            offsetGhost = 16;
			this.scatterDestination = new Phaser.Point(27 * this.gridsize, 0); 
            this.safetiles = [this.game.safetile];
            this.mode = this.SCATTER;
            break;
            
        default:
            break;
    }
    
    this.ghost = this.game.add.sprite((startPos.x * 16) + 8, (startPos.y * 16) + 8, key, 0);
    this.ghost.name = this.name;
    this.ghost.anchor.set(0.5);
    
	this.ghost.animations.add(Phaser.LEFT, [offsetGhost], 0, false);
    this.ghost.animations.add(Phaser.UP, [offsetGhost], 0, false);
    this.ghost.animations.add(Phaser.DOWN, [offsetGhost], 0, false);
    this.ghost.animations.add(Phaser.RIGHT, [offsetGhost], 0, false);
    this.ghost.animations.add("frightened", [offsetGhost+1], 10, true);
    this.ghost.animations.add(Phaser.RIGHT+20, [0], 0, false);
    this.ghost.animations.add(Phaser.LEFT+20, [1], 0, false);
    this.ghost.animations.add(Phaser.UP+20, [2], 0, false);
    this.ghost.animations.add(Phaser.DOWN+20, [3], 0, false);
	
    
    this.ghost.play(startDir);
    
    this.game.physics.arcade.enable(this.ghost);
    this.ghost.body.setSize(16, 16, 0, 0);
    
    this.move(startDir);
};

Ghost.prototype = {
    update: function() {
        if (this.mode !== this.RETURNING_HOME) {
            this.game.physics.arcade.collide(this.ghost, this.game.layer);
        }
        
        var x = this.game.math.snapToFloor(Math.floor(this.ghost.x), this.gridsize) / this.gridsize;
        var y = this.game.math.snapToFloor(Math.floor(this.ghost.y), this.gridsize) / this.gridsize;

        if (this.ghost.x < 0) {
            this.ghost.x = this.game.map.widthInPixels - 2;
        }
        if (this.ghost.x >= this.game.map.widthInPixels - 1) {
            this.ghost.x = 1;
        }
        
        if (this.isAttacking && (this.mode === this.SCATTER || this.mode === this.CHASE)) {
            this.ghostDestination = this.getGhostDestination();
            this.mode = this.CHASE;
        }
        
        if (this.game.math.fuzzyEqual((x * this.gridsize) + (this.gridsize /2), this.ghost.x, this.threshold) &&
           this.game.math.fuzzyEqual((y * this.gridsize) + (this.gridsize /2), this.ghost.y, this.threshold)) {
            //  Update our grid sensors
            this.directions[0] = this.game.map.getTile(x, y, this.game.layer);
            this.directions[1] = this.game.map.getTileLeft(this.game.layer.index, x, y) || this.directions[1];
            this.directions[2] = this.game.map.getTileRight(this.game.layer.index, x, y) || this.directions[2];
            this.directions[3] = this.game.map.getTileAbove(this.game.layer.index, x, y) || this.directions[3];
            this.directions[4] = this.game.map.getTileBelow(this.game.layer.index, x, y) || this.directions[4];
            
            var canContinue = this.checkSafetile(this.directions[this.currentDir].index);
            var possibleExits = [];
            for (var q=1; q<this.directions.length; q++) {
                if (this.checkSafetile(this.directions[q].index) && q !== this.opposites[this.currentDir]) {
                    possibleExits.push(q);
                }
            }
            switch (this.mode) {
                case this.RANDOM:
                    if (this.turnTimer < this.game.time.time && (possibleExits.length > 1 || !canContinue)) {
                        var select = Math.floor(Math.random() * possibleExits.length);
                        var newDirection = possibleExits[select];

                        this.turnPoint.x = (x * this.gridsize) + (this.gridsize / 2);
                        this.turnPoint.y = (y * this.gridsize) + (this.gridsize / 2);

                        // snap to grid exact position before turning
                        this.ghost.x = this.turnPoint.x;
                        this.ghost.y = this.turnPoint.y;

                        this.lastPosition = { x: x, y: y };
                        this.ghost.body.reset(this.turnPoint.x, this.turnPoint.y);
                        this.move(newDirection);

                        this.turnTimer = this.game.time.time + this.TURNING_COOLDOWN;
                    }
                    break;
                    
                case this.RETURNING_HOME:
                    if (this.turnTimer < this.game.time.time) {
                        this.ghost.body.reset(this.ghost.x, this.ghost.y);
                        if (this.flag = this.flag ? false : true) {
                            this.ghost.body.velocity.x = 0;
                            if (this.ghost.y < 14 * this.gridsize) {
                                this.ghost.body.velocity.y = this.cruiseElroySpeed;
                                this.ghost.animations.play(23);
                            }
                            if (this.ghost.y > 15 * this.gridsize) {
                                this.ghost.body.velocity.y  = -this.cruiseElroySpeed;
                                this.ghost.animations.play(22);
                            }
                        } else {
                            this.ghost.body.velocity.y = 0;
                            if (this.ghost.x < 13 * this.gridsize) {
                                this.ghost.body.velocity.x = this.cruiseElroySpeed;
                                this.ghost.animations.play(20);
                            }
                            if (this.ghost.x > 16 * this.gridsize) {
                                this.ghost.body.velocity.x = -this.cruiseElroySpeed;
                                this.ghost.animations.play(21);
                            }
                        }
                        this.turnTimer = this.game.time.time + this.RETURNING_COOLDOWN;
                    }
                    if (this.hasReachedHome()) {
                        this.turnPoint.x = (x * this.gridsize) + (this.gridsize / 2);
                        this.turnPoint.y = (y * this.gridsize) + (this.gridsize / 2);
                        this.ghost.x = this.turnPoint.x;
                        this.ghost.y = this.turnPoint.y;
                        this.ghost.body.reset(this.turnPoint.x, this.turnPoint.y);
                        this.mode = this.AT_HOME;
                        this.game.gimeMeExitOrder(this);
                    }
                    break;
                    
                case this.CHASE:
                    if (this.turnTimer < this.game.time.time) {
                        var distanceToObj = 999999;
                        var direction, decision, bestDecision;
                        for (q=0; q<possibleExits.length; q++) {
                            direction = possibleExits[q];
                            switch (direction) {
                                case Phaser.LEFT:
                                    decision = new Phaser.Point((x-1)*this.gridsize + (this.gridsize/2), 
                                                                (y * this.gridsize) + (this.gridsize / 2));
                                    break;
                                case Phaser.RIGHT:
                                    decision = new Phaser.Point((x+1)*this.gridsize + (this.gridsize/2), 
                                                                (y * this.gridsize) + (this.gridsize / 2));
                                    break;
                                case Phaser.UP:
                                    decision = new Phaser.Point(x * this.gridsize + (this.gridsize/2), 
                                                                ((y-1)*this.gridsize) + (this.gridsize / 2));
                                    break;
                                case Phaser.DOWN:
                                    decision = new Phaser.Point(x * this.gridsize + (this.gridsize/2), 
                                                                ((y+1)*this.gridsize) + (this.gridsize / 2));
                                    break;
                                default:
                                    break;
                            }
                            var dist = this.ghostDestination.distance(decision);
                            if (dist < distanceToObj) {
                                bestDecision = direction;
                                distanceToObj = dist;
                            }
                        }
                        
                        if (this.game.isSpecialTile({x: x, y: y}) && bestDecision === Phaser.UP) {
                            bestDecision = this.currentDir;
                        }

                        this.turnPoint.x = (x * this.gridsize) + (this.gridsize / 2);
                        this.turnPoint.y = (y * this.gridsize) + (this.gridsize / 2);

                        // snap to grid exact position before turning
                        this.ghost.x = this.turnPoint.x;
                        this.ghost.y = this.turnPoint.y;

                        this.lastPosition = { x: x, y: y };
                        
                        this.ghost.body.reset(this.turnPoint.x, this.turnPoint.y);
                        this.move(bestDecision);

                        this.turnTimer = this.game.time.time + this.TURNING_COOLDOWN;
                    }
                    break;
                    
                case this.AT_HOME:
                    if (!canContinue) {
                        this.turnPoint.x = (x * this.gridsize) + (this.gridsize / 2);
                        this.turnPoint.y = (14 * this.gridsize) + (this.gridsize / 2);
                        this.ghost.x = this.turnPoint.x;
                        this.ghost.y = this.turnPoint.y;
                        this.ghost.body.reset(this.turnPoint.x, this.turnPoint.y);
                        var dir = (this.currentDir === Phaser.LEFT) ? Phaser.RIGHT : Phaser.LEFT;
                        this.move(dir);
                    } else {
                        this.move(this.currentDir);
                    }
                    break;
                    
                case this.EXIT_HOME:
                    if (this.currentDir !== Phaser.UP && (x >= 13 || x <= 14)) {
                        this.turnPoint.x = (13 * this.gridsize) + (this.gridsize / 2);
                        this.turnPoint.y = (y * this.gridsize) + (this.gridsize / 2);
                        this.ghost.x = this.turnPoint.x;
                        this.ghost.y = this.turnPoint.y;
                        this.ghost.body.reset(this.turnPoint.x, this.turnPoint.y);                        
                        this.move(Phaser.UP);
                    } else if (this.currentDir === Phaser.UP && y == 11) {
                        this.turnPoint.x = (x * this.gridsize) + (this.gridsize / 2);
                        this.turnPoint.y = (y * this.gridsize) + (this.gridsize / 2);
                        this.ghost.x = this.turnPoint.x;
                        this.ghost.y = this.turnPoint.y;
                        this.ghost.body.reset(this.turnPoint.x, this.turnPoint.y);  
                        this.safetiles = [this.game.safetile];
                        this.mode = this.game.getCurrentMode();
                        return;
                    } else if (!canContinue) {
                        this.turnPoint.x = (x * this.gridsize) + (this.gridsize / 2);
                        this.turnPoint.y = (y * this.gridsize) + (this.gridsize / 2);
                        this.ghost.x = this.turnPoint.x;
                        this.ghost.y = this.turnPoint.y;
                        this.ghost.body.reset(this.turnPoint.x, this.turnPoint.y);
                        var dir = (this.currentDir === Phaser.LEFT) ? Phaser.RIGHT : Phaser.LEFT;
                        this.move(dir);
                    } 
                    break;
                    
                case this.SCATTER:
                    this.ghostDestination = new Phaser.Point(this.scatterDestination.x, this.scatterDestination.y);
                    this.mode = this.CHASE;
                    break;
                    
                case this.STOP:
                    this.move(Phaser.NONE);
                    break;
            }
        }
    },
    
    attack: function() {
        if (this.mode !== this.RETURNING_HOME) {
            this.isAttacking = true;
            this.ghost.animations.play(this.currentDir);
            if (this.mode !== this.AT_HOME && this.mode != this.EXIT_HOME) {
                this.currentDir = this.opposites[this.currentDir];
            }
        }
    },
    
    checkSafetile: function(tileIndex) {
        for (var q=0; q<this.safetiles.length; q++) {
            if (this.safetiles[q] == tileIndex) {
                return true;
            }
        }
        return false;
    },
    
    enterFrightenedMode: function() {
        if (this.mode !== this.AT_HOME && this.mode !== this.EXIT_HOME && this.mode !== this.RETURNING_HOME) {
            this.ghost.play("frightened");
            this.mode = this.RANDOM;
            this.isAttacking = false;
        }
    },
    
    getGhostDestination: function() {
        switch (this.name) {
            case "blinky":
                return this.game.pacman.getPosition();
                
            case "pinky":
                var dest = this.game.pacman.getPosition();
                var dir = this.game.pacman.getCurrentDirection();
                var offsetX = 0, offsetY = 0;
                if (dir === Phaser.LEFT || dir === Phaser.RIGHT) {
                    offsetX = (dir === Phaser.RIGHT) ? -4 : 4;
                }
                if (dir === Phaser.UP || dir === Phaser.DOWN) {
                    offsetY = (dir === Phaser.DOWN) ? -4 : 4;
                    if (dir === Phaser.UP && this.ORIGINAL_OVERFLOW_ERROR_ON) {
                        offsetX = 4;
                    }
                }
                offsetX *= this.gridsize;
                offsetY *= this.gridsize;
                dest.x -= offsetX;
                dest.y -= offsetY;
                if (dest.x < this.gridsize/2) dest.x = this.gridsize/2;
                if (dest.x > this.game.map.widthInPixels - this.gridsize/2) dest.x = this.game.map.widthInPixels - this.gridsize/2;
                if (dest.y < this.gridsize/2) dest.y = this.gridsize/2;
                if (dest.y > this.game.map.heightInPixels - this.gridsize/2) dest.y = this.game.map.heightInPixels - this.gridsize/2;
                return dest;
                
            case "inky":
                var pacmanPos = this.game.pacman.getPosition();
                var blinkyPos = this.game.blinky.getPosition();
                var diff = Phaser.Point.subtract(pacmanPos, blinkyPos);
                var dest = Phaser.Point.add(pacmanPos, diff);
                if (dest.x < this.gridsize/2) dest.x = this.gridsize/2;
                if (dest.x > this.game.map.widthInPixels - this.gridsize/2) dest.x = this.game.map.widthInPixels - this.gridsize/2;
                if (dest.y < this.gridsize/2) dest.y = this.gridsize/2;
                if (dest.y > this.game.map.heightInPixels - this.gridsize/2) dest.y = this.game.map.heightInPixels - this.gridsize/2;
                return dest;
                
            case "clyde":
                var pacmanPos = this.game.pacman.getPosition();
                var clydePos = this.getPosition();
                if (clydePos.distance(pacmanPos) > 8 * this.gridsize) {
                    return pacmanPos;
                } else {
                    return new Phaser.Point(this.scatterDestination.x, this.scatterDestination.y);
                }
                
            default:
                return new Phaser.Point(this.scatterDestination.x, this.scatterDestination.y);
        }
    },
    
    getPosition: function() {
        var x = this.game.math.snapToFloor(Math.floor(this.ghost.x), this.gridsize) / this.gridsize;
        var y = this.game.math.snapToFloor(Math.floor(this.ghost.y), this.gridsize) / this.gridsize;
        return new Phaser.Point((x * this.gridsize) + (this.gridsize / 2), (y * this.gridsize) + (this.gridsize / 2));
    },
    
    hasReachedHome: function() {
        if (this.ghost.x < 11*this.gridsize || this.ghost.x > 16*this.gridsize || 
            this.ghost.y < 13*this.gridsize || this.ghost.y > 15*this.gridsize) {
            return false;
        }
        return true;
    },
    
    move: function(dir) {
        this.currentDir = dir;
        
        var speed = this.ghostSpeed;
        if (this.game.getCurrentMode() === this.SCATTER) {
            speed = this.ghostScatterSpeed;
        }
        if (this.mode === this.RANDOM) {
            speed = this.ghostFrightenedSpeed;
        } else if (this.mode === this.RETURNING_HOME) {
            speed = this.cruiseElroySpeed;
            this.ghost.animations.play(dir+20);
        } else {
            this.ghost.animations.play(dir);
            if (this.name === "blinky" && this.game.numDots < 20) {
                speed = this.cruiseElroySpeed;
                this.mode = this.CHASE;   
            }
        }
        
        if (this.currentDir === Phaser.NONE) {
            this.ghost.body.velocity.x = this.ghost.body.velocity.y = 0;
            return;
        }
        
        if (dir === Phaser.LEFT || dir === Phaser.UP) {
            speed = -speed;
        }
        if (dir === Phaser.LEFT || dir === Phaser.RIGHT) {
            this.ghost.body.velocity.x = speed;
        } else {
            this.ghost.body.velocity.y = speed;
        }
    },
    
    resetSafeTiles: function() {
        this.safetiles = [this.game.safetile, 35, 36];
    },
    
    scatter: function() {
        if (this.mode !== this.RETURNING_HOME) {
            this.ghost.animations.play(this.currentDir);
            this.isAttacking = false;
            if (this.mode !== this.AT_HOME && this.mode != this.EXIT_HOME) {
                this.mode = this.SCATTER;
            }
        }
    }
};


var PacmanGame4 = function (game) {    
    this.map = null;
    this.layer = null;
    
    this.numDots = 0;
    this.TOTAL_DOTS = 0;
    this.score = 0;
    this.scoreText = null;
    
	this.battletime=0;
	this.timeText = null;
	
    this.pacman = null; 
    this.clyde = null;
    this.pinky = null;
    this.inky = null;
    this.blinky = null;
    this.isInkyOut = false;
    this.isClydeOut = false;
    this.ghosts = [];

    this.safetile = 14;
    this.gridsize = 16;       
    this.threshold = 3;
    
	this.ismusic=0;
	
    this.SPECIAL_TILES = [
        { x: 12, y: 11 },
        { x: 15, y: 11 },
        { x: 12, y: 23 },
        { x: 15, y: 23 }
    ];
    
    this.TIME_MODES = [
        {
            mode: "scatter",
            time: 7000
        },
        {
            mode: "chase",
            time: 20000
        },
        {
            mode: "scatter",
            time: 7000
        },
        {
            mode: "chase",
            time: 20000
        },
        {
            mode: "scatter",
            time: 5000
        },
        {
            mode: "chase",
            time: 20000
        },
        {
            mode: "scatter",
            time: 5000
        },
        {
            mode: "chase",
            time: -1 // -1 = infinite
        }
    ];
    this.changeModeTimer = 0;
    this.remainingTime = 0;
    this.currentMode = 0;
    this.isPaused = false;
    this.FRIGHTENED_MODE_TIME = 7000; //7second
    //this.ORIGINAL_OVERFLOW_ERROR_ON = true;
    //this.DEBUG_ON = true;
    
    this.KEY_COOLING_DOWN_TIME = 250;
    this.lastKeyPressed = 0;
    
    this.game = game;
};

PacmanGame4.prototype = {

    init: function () {
        

        Phaser.Canvas.setImageRenderingCrisp(this.game.canvas); // full retro mode, i guess ;)

        this.physics.startSystem(Phaser.Physics.ARCADE);
    },

    preload: function () {
        //  We need this because the assets are on Amazon S3
        //  Remove the next 2 lines if running locally
        //this.load.baseURL = 'http://files.phaser.io.s3.amazonaws.com/codingtips/issue005/';
        //this.load.crossOrigin = 'anonymous';
        game.stage.backgroundColor = "#90CAF9";

        game.load.image('dot', 'third/dot.png');
        game.load.image("C++", "assets/C++_small.png");
        game.load.image("html", "assets/html_small.png");
        game.load.image("css", "assets/css_small.png");
        game.load.image("JS", "assets/JS_small.png");
        game.load.image("vivado", "assets/vivado_small.png");
        game.load.image('tiles', 'third/pacman-tiles.png');
        game.load.spritesheet('pacman', 'third/simp.png', 32, 32);
        game.load.spritesheet("ghosts", "third/ghosts.png", 32, 32);

        game.load.tilemap('map', 'pacman-map.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.image('teacher1', 'third/teacher1.png');
        this.load.image('teacher2', 'assets/boss_b1.png');
        this.load.image('teacher3', 'assets/boss_b3.png');
        this.load.image('teacher4', 'assets/boss_b4.png');
        this.load.image('title', 'third/sopho1.png');

		//  Needless to say, the beast was stoned... and the graphics are Namco (C)opyrighted
    },

    create: function () {
        this.map = this.add.tilemap('map');
        this.map.addTilesetImage('pacman-tiles', 'tiles');

        this.layer = this.map.createLayer('Pacman');

        this.dots = this.add.physicsGroup();
        this.numDots = this.map.createFromTiles(7, this.safetile, 'dot', this.layer, this.dots);
        this.TOTAL_DOTS = this.numDots;
        
        this.pills = this.add.physicsGroup();
        this.numPills = this.map.createFromTiles(41, this.safetile, "C++", this.layer, this.pills);
        this.numPills += this.map.createFromTiles(40, this.safetile, "html", this.layer, this.pills);
        this.numPills += this.map.createFromTiles(39, this.safetile, "css", this.layer, this.pills);
        this.numPills += this.map.createFromTiles(38, this.safetile, "JS", this.layer, this.pills);
        this.numPills += this.map.createFromTiles(37, this.safetile, "vivado", this.layer, this.pills);

        //  The dots will need to be offset by 6px to put them back in the middle of the grid
        this.dots.setAll('x', 6, false, false, 1);
        this.dots.setAll('y', 6, false, false, 1);

        //  Pacman should collide with everything except the safe tile
        this.map.setCollisionByExclusion([this.safetile], true, this.layer);

		// Our hero
        this.pacman = new Pacman(this, "pacman");

        // Score and debug texts
        this.scoreText = game.add.text(8, 272, "Score: " + this.score, { fontSize: "16px", fill: "#fff" });
        //this.debugText = game.add.text(375, 260, "", { fontSize: "12px", fill: "#fff" });
        //this.overflowText = game.add.text(375, 280, "", { fontSize: "12px", fill: "#fff" });
        
		this.timeText = game.add.text(370, 272, "Battle: " + this.battletime, { fontSize: "16px", fill: "#fff" });
		
        this.cursors = this.input.keyboard.createCursorKeys();
        //this.cursors["d"] = this.input.keyboard.addKey(Phaser.Keyboard.D);
        //this.cursors["b"] = this.input.keyboard.addKey(Phaser.Keyboard.B);
        
        //this.game.time.events.add(1250, this.sendExitOrder, this);
        //this.game.time.events.add(7000, this.sendAttackOrder, this);
        
        this.changeModeTimer = this.time.time + this.TIME_MODES[this.currentMode].time;
        
        // Ghosts
        this.blinky = new Ghost(this, "ghosts", "blinky", {x:13, y:11}, Phaser.RIGHT);
        this.pinky = new Ghost(this, "ghosts", "pinky", {x:15, y:14}, Phaser.LEFT);
        this.inky = new Ghost(this, "ghosts", "inky", {x:14, y:14}, Phaser.RIGHT);
        this.clyde = new Ghost(this, "ghosts", "clyde", {x:17, y:14}, Phaser.LEFT);
        this.ghosts.push(this.clyde, this.pinky, this.inky, this.blinky);
        
        this.sendExitOrder(this.pinky);
		
		//music
		this.BGMSound =  game.add.audio('BGM',1,true);
		this.bigpointSound =  game.add.audio('bigpoint');
		this.HitbossSound = game.add.audio('Hitboss');
        this.deadSound = game.add.audio('dead');

        this.title= game.add.sprite(game.width/2+228, 40, 'title');
        this.title.anchor.setTo(0.5,0.5);
        this.title.scale.setTo(0.75,0.75);
        
        this.ty= game.add.sprite(game.width/2+150, game.height/2-160, 'teacher2');
        this.ty.scale.setTo(0.6,0.6);
        this.ty1Text = game.add.text(game.width/2+235, game.height/2-150, "黃稚存教授", { fontSize: "16px", fill: "#fff" });
        this.ty2Text = game.add.text(game.width/2+235, game.height/2-130, "硬體實驗", { fontSize: "16px", fill: "#fff" });

        this.hj= game.add.sprite(game.width/2+150, game.height/2-60, 'teacher3');
        this.hj.scale.setTo(0.6,0.6);
        this.hj1Text = game.add.text(game.width/2+235, game.height/2-50, "李哲榮教授", { fontSize: "16px", fill: "#fff" });
        this.hj2Text = game.add.text(game.width/2+235, game.height/2-30, "程式設計", { fontSize: "16px", fill: "#fff" });

        this.jy= game.add.sprite(game.width/2+150, game.height/2+40, 'teacher1');
        this.jy.scale.setTo(0.2,0.2);
        this.jy1Text = game.add.text(game.width/2+235, game.height/2+50, "李濬屹教授", { fontSize: "16px", fill: "#fff" });
        this.jy2Text = game.add.text(game.width/2+235, game.height/2+70, "硬體實驗", { fontSize: "16px", fill: "#fff" });

        this.sr= game.add.sprite(game.width/2+150, game.height/2+140, 'teacher4');
        this.sr.scale.setTo(0.6,0.6);
        this.sr1Text = game.add.text(game.width/2+235, game.height/2+150, "林永隆教授", { fontSize: "16px", fill: "#fff" });
        this.sr2Text = game.add.text(game.width/2+235, game.height/2+170, "邏輯設計", { fontSize: "16px", fill: "#fff" });
    },

    checkKeys: function () {
        this.pacman.checkKeys(this.cursors);
        
    },
    
    checkMouse: function() {
        if (this.input.mousePointer.isDown) {            
            var x = this.game.math.snapToFloor(Math.floor(this.input.x), this.gridsize) / this.gridsize;
            var y = this.game.math.snapToFloor(Math.floor(this.input.y), this.gridsize) / this.gridsize;
            this.debugPosition = new Phaser.Point(x * this.gridsize, y * this.gridsize);
            console.log(x, y);
        }
    },
    
    dogEatsDog: function(pacman, ghost) {
        if (this.isPaused) {
			this.HitbossSound.play();//music here
            this[ghost.name].mode = this[ghost.name].RETURNING_HOME;
            this[ghost.name].ghostDestination = new Phaser.Point(14 * this.gridsize, 14 * this.gridsize);
            this[ghost.name].resetSafeTiles();
            this.score += 10;
        } else {
            this.deadSound.play();
            this.BGMSound.stop();
            this.killPacman();
            // game.state.start('next');
        }
    },
    
    getCurrentMode: function() {
        if (!this.isPaused) {
            if (this.TIME_MODES[this.currentMode].mode === "scatter") {
                return "scatter";
            } else {
                return "chase";
            }
        } else {
            return "random";
        }
    },
    
    gimeMeExitOrder: function(ghost) {
        this.game.time.events.add(Math.random() * 3000, this.sendExitOrder, this, ghost);
    },
        
    killPacman: function() {
        this.pacman.isDead = true;
        this.stopGhosts();
    },
    
    stopGhosts: function() {
        for (var i=0; i<this.ghosts.length; i++) {
            this.ghosts[i].mode = this.ghosts[i].STOP;
        }
    },

    update: function () {	
        if(this.ismusic==0){
			this.BGMSound.play();
			this.ismusic=1;
		}
		this.scoreText.text = "Score: " + this.score;
		
		this.timeText.text = "Battle: "+this.battletime;
        if(this.battletime>0)this.battletime-=1;
        
        if (!this.pacman.isDead) {
            for (var i=0; i<this.ghosts.length; i++) {
                if (this.ghosts[i].mode !== this.ghosts[i].RETURNING_HOME) {
                    this.physics.arcade.overlap(this.pacman.sprite, this.ghosts[i].ghost, this.dogEatsDog, null, this);
                }
            }
            
            if (this.TOTAL_DOTS - this.numDots > 30 && !this.isInkyOut) {
                this.isInkyOut = true;
                this.sendExitOrder(this.inky);
            }
            
            if (this.numDots < this.TOTAL_DOTS/3 && !this.isClydeOut) {
                this.isClydeOut = true;
                this.sendExitOrder(this.clyde);
            }
            
            if (this.changeModeTimer !== -1 && !this.isPaused && this.changeModeTimer < this.time.time) {
                this.currentMode++;
                this.changeModeTimer = this.time.time + this.TIME_MODES[this.currentMode].time;
                if (this.TIME_MODES[this.currentMode].mode === "chase") {
                    this.sendAttackOrder();
                } else {
                    this.sendScatterOrder();
                }
                console.log("new mode:", this.TIME_MODES[this.currentMode].mode, this.TIME_MODES[this.currentMode].time);
            }
            if (this.isPaused && this.changeModeTimer < this.time.time) {
                this.changeModeTimer = this.time.time + this.remainingTime;
                this.isPaused = false;
                if (this.TIME_MODES[this.currentMode].mode === "chase") {
                    this.sendAttackOrder();
                } else {
                    this.sendScatterOrder();
                }
                console.log("new mode:", this.TIME_MODES[this.currentMode].mode, this.TIME_MODES[this.currentMode].time);
            }
        }else{
            this.gameover();
        }
        
        this.pacman.update();
		this.updateGhosts();
        
        this.checkKeys();
        this.checkMouse();
    },
    gameover: function(){
        game.state.start('over4');
        var score = this.score;

        firebase.database().ref('temp/').once('value').then(function(snapshot){
            if(snapshot.exists()){
                tempName = snapshot.val().name;

                firebase.database().ref('fourth/'+ tempName).once('value').then(function(snapshot){
                    if(snapshot.exists()){
                        high = snapshot.val().score;
                        //console.log(tempName,high);
                        if(high>score) score = high;
                        var newitem = firebase.database().ref('fourth/'+ tempName);
                        newitem.set({
                            name: tempName,
                            score: score
                        })
                    }else{
                        var newitem = firebase.database().ref('fourth/'+ tempName);
                        newitem.set({
                            name: tempName,
                            score: score
                        })
                    }
                });

            }else{
                var newitem = firebase.database().ref('third/' +tempName);
                newitem.set({
                    name: tempName,
                    score: score
                }) 
            }
            
        });
    },
    enterFrightenedMode: function() {
        for (var i=0; i<this.ghosts.length; i++) {
            this.ghosts[i].enterFrightenedMode();
        }
        if (!this.isPaused) {
            this.remainingTime = this.changeModeTimer - this.time.time;
        }
        this.changeModeTimer = this.time.time + this.FRIGHTENED_MODE_TIME;
		this.battletime=409;
        this.isPaused = true;
		this.bigpointSound.play();
        console.log(this.remainingTime);
    },
    
    isSpecialTile: function(tile) {
        for (var q=0; q<this.SPECIAL_TILES.length; q++) {
            if (tile.x === this.SPECIAL_TILES[q].x && tile.y === this.SPECIAL_TILES[q].y) {
                return true;
            } 
        }
        return false;
    },
    
    updateGhosts: function() {
        for (var i=0; i<this.ghosts.length; i++) {
            this.ghosts[i].update();
        }
    },
    
    
    
    sendAttackOrder: function() {
        for (var i=0; i<this.ghosts.length; i++) {
            this.ghosts[i].attack();
        }
    },
    
    sendExitOrder: function(ghost) {
        ghost.mode = this.clyde.EXIT_HOME;
    },
    
    sendScatterOrder: function() {
        for (var i=0; i<this.ghosts.length; i++) {
            this.ghosts[i].scatter();
        }
    },

    loadJSON(callback) {   
        var xobj = new XMLHttpRequest();
            xobj.overrideMimeType("application/json");
        xobj.open('GET', 'pacman-map.json', true); // Replace 'my_data' with the path to your file
        xobj.onreadystatechange = function () {
              if (xobj.readyState == 4 && xobj.status == "200") {
                // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                callback(xobj.responseText);
              }
        };
        xobj.send(null);  
     }
};


