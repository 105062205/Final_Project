var loadState = {
    preload: function(){
        game.stage.backgroundColor = "#330939";
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' }); 
        loadingLabel.anchor.setTo(0.5, 0.5);
    
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);

        
		
		//MUSIC
		game.load.audio('BGM', ['assets/Jay_Jay3.ogg', 'assets/Jay_Jay3.mp3']);
		game.load.audio('bigpoint', ['assets/bigpoint.ogg', 'assets/bigpoint.mp3']);
		game.load.audio('Hitboss', ['assets/Hit_boss.ogg', 'assets/Hit_boss.mp3']);		
        game.load.audio('dead', ['assets/die.ogg', 'assets/die.mp3']);
        game.load.image('menulabel', 'assets/menulabel.png');
        game.load.image('gametitle', 'share/gametitle.png');
        game.load.image('gametitle1', 'share/gametitle_1.png');
        game.load.image('GAMEOVER1', 'share/OVER1.png');

    },
    create: function(){
        // Go to the menu state 
        game.state.start('menu');
    }
};