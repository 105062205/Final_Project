// // Initialize Phaser
var game = new Phaser.Game(700, 496, Phaser.AUTO, 'canvas'); 
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);

game.state.add('over1', overState1);
game.state.add('over2', overState2);
game.state.add('over3', overState3);
game.state.add('over4', overState4);

// game.state.add('Game1', PacmanGame1, true);
// game.state.add('Game2', PacmanGame2, true);
// game.state.add('Game3', PacmanGame3, true);
game.state.start('boot');
